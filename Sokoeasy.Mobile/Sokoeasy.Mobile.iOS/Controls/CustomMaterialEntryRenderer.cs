﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using Sokoeasy.Mobile.Controls;
using Sokoeasy.Mobile.iOS.Controls;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomMaterialEntry), typeof(CustomMaterialEntryRenderer))]
namespace Sokoeasy.Mobile.iOS.Controls
{
    public class CustomMaterialEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.BorderStyle = UITextBorderStyle.None;
                Control.Layer.CornerRadius = 10;
                Control.TextColor = UIColor.White;
            }
        }
    }
}