﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Firebase.Auth;
using Foundation;
using UIKit;
using Xamarin.Essentials;
using Sokoeasy.Mobile.Services.Interfaces;

namespace Sokoeasy.Mobile.iOS.PlatformServices
{
    public class iOSPhoneAuth : IAuth
    {
        private string verificationCode = "";
        public Task Login(string phonenumber)
        {
            Auth.DefaultInstance.LanguageCode = "fr";
            PhoneAuthProvider.DefaultInstance.VerifyPhoneNumber(phonenumber, null, verificationHandler);

            return Task.CompletedTask;
        }

        private void verificationHandler(string verificationId, NSError error)
        {
            var credential = PhoneAuthProvider.DefaultInstance
                .GetCredential(verificationId, verificationCode);

            var assertion = PhoneMultiFactorGenerator.GetAssertion(credential);

            Auth.DefaultInstance.SignInWithCredential(credential, authResult);
        }

        private void authResult(AuthDataResult authResult, NSError error)
        {
        }
    }
}