﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoeasy.Models
{
    public class Vegetable
    {
        public string Name { get; set; }

        public double Price { get; set; }

        public bool IsAvailable { get; set; }

        public Category Category { get; set; }
    }
}
