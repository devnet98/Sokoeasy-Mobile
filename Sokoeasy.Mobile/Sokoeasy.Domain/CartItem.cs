﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoeasy.Domain
{
    public class CartItem
    {
        public Vegetable Product { get; set; }

        public int Quantity { get; set; }

        public double Price => Product.Price;

        public double TotalPrice => Price * Quantity;
    }
}
