﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoeasy.Domain
{
    public class User
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string ProfileImage { get; set; }

        public bool IsAdministrator { get; set; }
    }
}
