﻿using System;

namespace Sokoeasy.Domain
{
    public class Vegetable
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string ImageUrl { get; set; }

        public bool IsAvailable { get; set; }

        public Category Category { get; set; }
    }
}
