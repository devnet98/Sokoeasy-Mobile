﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sokoeasy.Domain
{
    public class Order
    {
        public string Id { get; set; }

        public List<CartItem> ProductItems { get; set; }

        public User User { get; set; }

        public int OrderState { get; set; }

        public double TotalPrice => ProductItems.Sum(c => c.Price);
        //En attente de confirmation, Préparation de la livraison, En route pour la livraison, Commande livré
    }
}
