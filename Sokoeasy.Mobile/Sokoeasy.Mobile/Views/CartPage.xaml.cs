﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Badge.Abstractions;
using Sokoeasy.Mobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sokoeasy.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CartPage : ContentPage
    {
        public CartPage()
        {
            InitializeComponent();

            MessagingCenter.Instance.Subscribe<GlobalCart>(this,"CartItemChanged", (sender) =>
            {
                int cartItemsCount = sender.CartItems.Sum(c => c.Quantity);
                TabBadge.SetBadgeText(this, cartItemsCount != 0 ? (cartItemsCount).ToString() : null);
            });
        }

        private void SwipeItem_OnInvoked(object sender, EventArgs e)
        {
            var cartItemToRemove = ((SwipeItem)sender).BindingContext as CartItem;
            GlobalCart.Instance.CartItems.Remove(cartItemToRemove);
        }
    }
}