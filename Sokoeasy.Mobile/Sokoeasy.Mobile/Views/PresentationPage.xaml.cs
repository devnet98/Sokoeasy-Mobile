﻿using Sokoeasy.Mobile.Controls;
using Sokoeasy.Mobile.Enums;
using Sokoeasy.Mobile.Messaging;
using Sokoeasy.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sokoeasy.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PresentationPage : ContentPage
    {
        public PresentationPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<PresentationPageViewModel, TransitionType>(this, MessagingTokens.TransitionMessage, async (sender, arg) =>
            {
                var transitionType = (TransitionType)arg;
                var transitionNavigationPage = Parent as NavigationPageAnimation;

                if (transitionNavigationPage != null)
                {
                    transitionNavigationPage.TransitionType = transitionType;
                    
                    var lastPage = Navigation.NavigationStack.LastOrDefault();
                    if (lastPage.GetType() == typeof(RegisterPage)) return;
                    
                    await Navigation.PushAsync(new RegisterPage());
                }
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MessagingCenter.Unsubscribe<RegisterPageViewModel, TransitionType>(this, MessagingTokens.TransitionMessage);
        }
    }
}