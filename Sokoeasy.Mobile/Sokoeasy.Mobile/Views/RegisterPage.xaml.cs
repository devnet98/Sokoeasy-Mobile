﻿using Sokoeasy.Mobile.Controls;
using Sokoeasy.Mobile.Enums;
using Sokoeasy.Mobile.Messaging;
using Sokoeasy.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sokoeasy.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //MessagingCenter.Subscribe<RegisterPageViewModel, TransitionType>(this, MessagingTokens.TransitionMessage, (sender, arg) =>
            //{
            //    var transitionType = (TransitionType)arg;
            //    var transitionNavigationPage = Parent as NavigationPageAnimation;

            //    if (transitionNavigationPage != null)
            //    {
            //        transitionNavigationPage.TransitionType = transitionType;
            //        Navigation.PushAsync(new VerificationPage());
            //    }
            //});
        }
    }
}