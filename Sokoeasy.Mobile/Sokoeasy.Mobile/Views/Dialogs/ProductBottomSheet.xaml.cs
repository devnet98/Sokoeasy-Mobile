﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sokoeasy.Mobile.Views.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductBottomSheet : ContentView
    {
        double x,y;
        double factorScale = 0;

        public ProductBottomSheet()
        {
            InitializeComponent();
            SetInitialPositionPages();
        }

        private void SetInitialPositionPages()
        {
            //RelativeLayout.SetHeightConstraint(contentview, Constraint.RelativeToParent((parent) =>
            //{
            //    return parent.Height * .8;
            //}));

            RelativeLayout.SetYConstraint(frame, Constraint.RelativeToParent((parent) =>
            {
                return parent.Height * .8-100;
            }));
        }


        void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            // Handle the pan
            switch (e.StatusType)
            {
                case GestureStatus.Running:
                    var YTranslation = y + e.TotalY;
                    // Translate and ensure we don't pan beyond the wrapped user interface element bounds.

                    var framePos = frame.Height * .8;
                    var finalPos = framePos + YTranslation;
                    factorScale = finalPos / Height;

                    if (factorScale > -0.03 && factorScale < .95)
                    {
                        //frame.TranslationY = YTranslation;
                        frame.TranslateTo(frame.TranslationX, YTranslation, 100, Easing.SinOut);
                        //if (factorScale > .3)
                        //{
                        //    RelativeLayout.SetHeightConstraint(contentview, Constraint.RelativeToParent((parent) =>
                        //    {
                        //        return parent.Height * factorScale;
                        //    }));
                        //}
                    }

                    break;

                case GestureStatus.Completed:
                    // Store the translation applied during the pan
                    var initFramePos = Height * .8;
                    var finalFramePos = initFramePos + frame.TranslationY;
                    factorScale = finalFramePos / Height;
                    if (factorScale > .8)
                    {
                        var ytrans = (Height * (1 - .8)) + 75;
                        frame.TranslateTo(0, ytrans, 300, Easing.BounceOut);
                        y = ytrans;
                        //RelativeLayout.SetHeightConstraint(contentview, Constraint.RelativeToParent((parent) =>
                        //{
                        //    return parent.Height;
                        //}));
                        //expandeButton.FadeTo(1, 500, Easing.CubicIn);
                    }
                    else
                    {
                        y = frame.TranslationY;
                    }
                    break;
            }

        }
    }
}