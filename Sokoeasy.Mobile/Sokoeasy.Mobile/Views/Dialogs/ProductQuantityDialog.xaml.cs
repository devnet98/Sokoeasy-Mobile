﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Sokoeasy.Mobile.Models;
using Sokoeasy.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Vegetable = Sokoeasy.Mobile.Models.Vegetable;

namespace Sokoeasy.Mobile.Views.Dialogs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductQuantityDialog : PopupPage
    {
        public Vegetable Vegetable { get; set; }

        private double x,y;

        public ProductQuantityDialog(CartItem product)
        {
            InitializeComponent();
            BindingContext = (ProductQuantityDialogViewModel) BindingContext ?? new ProductQuantityDialogViewModel(product);
        }

        
        //void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        //{
        //    // Handle the pan
        //    switch (e.StatusType)
        //    {
        //        case GestureStatus.Running:
        //            var translateY = e.TotalY;
        //            bottomSheet.TranslateTo(bottomSheet.X, translateY, 10);
        //            break;
        //        case GestureStatus.Completed:
        //            y = bottomSheet.TranslationY;
        //            break;
        //    }

        //}
    }
}