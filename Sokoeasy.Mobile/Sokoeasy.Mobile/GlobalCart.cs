﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Prism.Mvvm;
using Sokoeasy.Mobile.Models;
using Xamarin.Forms;

namespace Sokoeasy.Mobile
{
    public class GlobalCart : BindableBase
    {
        private GlobalCart()
        {
            CartItems = new ObservableCollection<CartItem>();
            CartItems.CollectionChanged += OnCartItemsCollectionChanged;
        }

        public ObservableCollection<CartItem> CartItems { get; set; }

        private static GlobalCart globalCart;

        public static GlobalCart Instance
        {
            get
            {
                lock (typeof(GlobalCart))
                {
                    if (globalCart == null)
                    {
                        GlobalCart.globalCart = new GlobalCart();
                    }
                }

                return globalCart;
            }

            set
            {
                lock (typeof(GlobalCart))
                {
                    GlobalCart.globalCart = value;
                }
            }
        }

        private void OnCartItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.NewItems != null)
                foreach (CartItem item in args.NewItems)
                    item.PropertyChanged += CartItemPropertyChanged;

            if (args.OldItems != null)
                foreach (CartItem item in args.OldItems)
                    item.PropertyChanged -= CartItemPropertyChanged;

            MessagingCenter.Instance.Send(this,"CartItemChanged");

        }

        private void CartItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            MessagingCenter.Instance.Send(this,"CartItemChanged");

        }

    }
}
