﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sokoeasy.Mobile.Services.Interfaces
{
    public interface IAuth
    {
        Task Login(string phonenumber);
    }
}
