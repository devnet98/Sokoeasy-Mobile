using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Prism;
using Prism.Ioc;
using Prism.Plugin.Popups;
using Sokoeasy.Domain;
using Sokoeasy.Mobile.ViewModels;
using Sokoeasy.Mobile.Views;
using Sokoeasy.Mobile.Views.Dialogs;
using Xamarin.Essentials.Interfaces;
using Xamarin.Essentials.Implementation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Sokoeasy.Core.Services.Abstractions;
using Sokoeasy.Core.Services;
using Sokoeasy.Core.Services.FakeServices;
using Sokoeasy.Mobile.Services.Interfaces;
using Sokoeasy.Mobile.Controls;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("SourceSansPro-Black.ttf", Alias = "SourceSansProBlack")]
[assembly: ExportFont("SourceSansPro-BlackItalic.ttf", Alias = "SourceSansProBlackItalic")]
[assembly: ExportFont("SourceSansPro-Bold.ttf", Alias = "SourceSansProBold")]
[assembly: ExportFont("SourceSansPro-BoldItalic.ttf", Alias = "SourceSansProBoldItalic")]
[assembly: ExportFont("SourceSansPro-ExtraLight.ttf", Alias = "SourceSansProExtraLight")]
[assembly: ExportFont("SourceSansPro-ExtraLightItalic.ttf", Alias = "SourceSansProExtraLightItalic")]
[assembly: ExportFont("SourceSansPro-Italic.ttf", Alias = "SourceSansProItalic")]
[assembly: ExportFont("SourceSansPro-Light.ttf", Alias = "SourceSansProLight")]
[assembly: ExportFont("SourceSansPro-LightItalic.ttf", Alias = "SourceSansProLightItalic")]
[assembly: ExportFont("SourceSansPro-Regular.ttf", Alias = "SourceSansProRegular")]
[assembly: ExportFont("SourceSansPro-Semibold.ttf", Alias = "SourceSansProSemibold")]
[assembly: ExportFont("SourceSansPro-SemiboldItalic.ttf", Alias = "SourceSansProSemiboldItalic")]
[assembly: ExportFont("FontAwesomeSolid.ttf", Alias = "FontAwesome")]
namespace Sokoeasy.Mobile
{
    public partial class App
    {

        public static ObservableCollection<CartItem> CartItems { get; set; }
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            Device.SetFlags(new string[]{ "MediaElement_Experimental", "SwipeView_Experimental" });
            await NavigationService.NavigateAsync($"{nameof(NavigationPageAnimation)}/{nameof(MainPage)}");
            //MainPage = new NavigationPageAnimation(new PhoneConnexionPage());
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();

            containerRegistry.Register<IUserServices, UserServices>();
            containerRegistry.RegisterForNavigation<NavigationPageAnimation>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<CartPage, CartPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<StoresPage, StoresPageViewModel>();
            containerRegistry.RegisterForNavigation<DetailPage, DetailPageViewModel>();
            containerRegistry.RegisterForNavigation<RegisterPage, RegisterPageViewModel>();
            containerRegistry.RegisterForNavigation<PresentationPage, PresentationPageViewModel>();
            containerRegistry.RegisterForNavigation<VerificationPage, VerificationPageViewModel>();
            containerRegistry.RegisterForNavigation<PhoneConnexionPage, PhoneConnexionPageViewModel>();
            containerRegistry.RegisterForNavigation<PhoneVerificationPage, PhoneVerificationPageViewModel>();

            containerRegistry.RegisterPopupNavigationService();
        }

        
    }
}
