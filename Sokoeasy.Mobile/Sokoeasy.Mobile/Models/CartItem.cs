﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;
using Sokoeasy.Domain;

namespace Sokoeasy.Mobile.Models
{
    public class CartItem : BindableBase
    {
        private int _quantity = 0;

        public Vegetable Product { get; set; }

        public int Quantity
        {
            get => _quantity;
            set
            {
                SetProperty(ref _quantity, value);
                RaisePropertyChanged(nameof(TotalPrice));
            }
        }

        public double Price => Product.Price;

        public double TotalPrice => Price * Quantity;
    }
}
