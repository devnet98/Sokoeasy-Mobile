﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoeasy.Mobile.Models
{
    public class Country
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }
}
