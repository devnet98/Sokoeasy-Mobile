﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoeasy.Mobile.Messaging
{
    public static class MessagingTokens
    {
        public const string AuthVerificationCompleted = "AuthVerificationCompleted";
        public const string TransitionMessage = "Transition";
    }
}
