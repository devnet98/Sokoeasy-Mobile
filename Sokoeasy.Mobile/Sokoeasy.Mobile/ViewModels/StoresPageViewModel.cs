﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Prism.Navigation;
using Prism.Services.Dialogs;
using Rg.Plugins.Popup.Services;
using Sokoeasy.Mobile.Views.Dialogs;
using Sokoeasy.Mobile.Models;
using Xamarin.Forms;
using Sokoeasy.Domain;

namespace Sokoeasy.Mobile.ViewModels
{
    public class StoresPageViewModel : ViewModelBase
    {
        public ObservableCollection<Models.Vegetable> Legumes { get; set; }
        public ObservableCollection<Models.Vegetable> Epices { get; set; }

        public Models.Vegetable LegumeProduct { get; set; }

        public ICommand ShowItemDetailCommand => new Command( (product) 
            => showItemDetail((Models.Vegetable)product));

        public StoresPageViewModel(INavigationService navigationService, IDialogService dialogService)
            : base(navigationService, dialogService)
        {
            Title = "Marché";
            InitSampleData();
        }

        private void InitSampleData()
        {
            Category legume = new Category()
            {
                Name = "Légume"
            };

            Category epice = new Category()
            {
                Name = "Epice"
            };

            Category fruit = new Category()
            {
                Name = "Fruit"
            };

            Legumes = new ObservableCollection<Models.Vegetable>()
            {
                new Models.Vegetable()
                {
                    Name = "Feuille de manioc (Pondu)",
                    Price = 800,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Legumes.Sombe.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },
                new Models.Vegetable()
                {
                    Name = "Gombo (Mulenda)",
                    Price = 350,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Legumes.Gombo.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },

                new Models.Vegetable()
                {
                    Name = "Choux de chine",
                    Price = 1200,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Legumes.ChouxChine.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },

                new Models.Vegetable()
                {
                    Name = "Amaranthe (Lenga Lenga)",
                    Price = 950,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Legumes.LengaLenga.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },
                new Models.Vegetable()
                {
                    Name = "Haricot Rouge (Malaki)",
                    Price = 3500,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Legumes.HaricotRouge.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },
                new Models.Vegetable()
                {
                    Name = "Haricot Blanc (Malaki)",
                    Price = 4800,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Legumes.HaricotBlanc.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                }
            };

            Epices = new ObservableCollection<Models.Vegetable>()
            {
                new Models.Vegetable()
                {
                    Name = "Aille",
                    Price = 100,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Epices.Aille.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },
                new Models.Vegetable()
                {
                    Name = "Carotte",
                    Price = 700,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Epices.Carotte.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand

                },

                new Models.Vegetable()
                {
                    Name = "Celerie",
                    Price = 250,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Epices.Celerie.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },

                new Models.Vegetable()
                {
                    Name = "Piment (Pili Pili)",
                    Price = 50,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Epices.Piment.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },
                new Models.Vegetable()
                {
                    Name = "Poivron",
                    Price = 500,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Epices.Poivron.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                },
                new Models.Vegetable()
                {
                    Name = "Tomate",
                    Price = 600,
                    Category = legume,
                    ImageUrl = "resource://Sokoeasy.Mobile.Resources.SokoeasyPicture.Epices.Tomate.png",
                    ShowItemDetailCommand = this.ShowItemDetailCommand
                }
            };
        }

        private void showItemDetail(Models.Vegetable vegetable)
        {
            PopupNavigation.Instance.PushAsync(new ProductQuantityDialog(new Models.CartItem(){Product = vegetable, Quantity = 0}), true);
        }
    }
}
