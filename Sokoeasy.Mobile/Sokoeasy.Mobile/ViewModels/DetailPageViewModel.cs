﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Navigation;
using Prism.Services.Dialogs;

namespace Sokoeasy.Mobile.ViewModels
{
    public class DetailPageViewModel : ViewModelBase
    {
        public DetailPageViewModel(INavigationService navigationService, IDialogService dialogService)
            : base(navigationService, dialogService)
        {
            Title = "Main Page";
        }
    }
}
