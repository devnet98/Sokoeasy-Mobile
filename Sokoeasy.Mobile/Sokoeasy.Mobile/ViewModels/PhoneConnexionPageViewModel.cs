﻿using Prism.Navigation;
using Prism.Services.Dialogs;
using Rg.Plugins.Popup.Contracts;
using Sokoeasy.Core.Services.Abstractions;
using Sokoeasy.Mobile.Dialogs;
using Sokoeasy.Mobile.Models;
using Sokoeasy.Mobile.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Sokoeasy.Mobile.ViewModels
{
    public class PhoneConnexionPageViewModel : ViewModelBase
    {
        private List<Country> _countries;
        private Country _selectedCountry;
        private string _countryCode;
        private string _phoneNumber;

        private readonly IUserServices _userServices;
        private readonly IPopupNavigation _popupNavigation;
        private readonly IAuth _auth;

        public List<Country> Countries
        {
            get => _countries;
            set => SetProperty(ref _countries, value);
        }

        public Country SelectedCountry
        {
            get => _selectedCountry;
            set
            {
                SetProperty(ref _selectedCountry, value);
                CountryCode = _selectedCountry.Code;
            }
        }

        public string CountryCode
        {
            get => _countryCode;
            set => SetProperty(ref _countryCode, value);
        }

        public string PhoneNumber
        {
            get => _phoneNumber;
            set => SetProperty(ref _phoneNumber, value);
        }

        public Command LoginWithPhoneNumberCommand => new Command(() => loginWithPhoneNumberCommand());

        public PhoneConnexionPageViewModel(INavigationService navigationService, IDialogService dialogService,
            IUserServices userServices, IAuth auth, IPopupNavigation popupNavigation) : base(navigationService, dialogService)
        {
            _userServices = userServices;
            _popupNavigation = popupNavigation;
            _auth = auth;

            InitializeCountries();
        }

        private void InitializeCountries()
        {
            Countries = new List<Country>();
            Countries.Add(new Country()
            {
                Code = "243",
                Name = "République Démocratique du Congo"
            });

            SelectedCountry = Countries[0];
        }

        private async void loginWithPhoneNumberCommand()
        {
            var fullPhoneNumber = $"+{SelectedCountry.Code}{PhoneNumber}";
            var connectedUser =  await _userServices.GetUserByPhoneNumber(fullPhoneNumber);

            if (connectedUser != null)
            {
                await _popupNavigation.PushAsync(new ConfirmAccountDialog(), true);
                //await Application.Current.MainPage.DisplayAlert("Authentification", $"Bienvenue {connectedUser}!", "OK");
            }
            else
            {
                await _popupNavigation.PushAsync(new ConfirmAccountDialog(), true);
                //await Application.Current.MainPage.DisplayAlert("Authentification", $"Renseignez vos informations", "OK");
            }
        }
    }
}
