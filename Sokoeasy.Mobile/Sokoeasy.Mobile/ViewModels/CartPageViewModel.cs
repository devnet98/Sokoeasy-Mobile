﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Prism.Navigation;
using Prism.Services.Dialogs;
using Xamarin.Forms;
using System.Linq;
using Plugin.Badge.Abstractions;
using Sokoeasy.Mobile.Models;

namespace Sokoeasy.Mobile.ViewModels
{
    public class CartPageViewModel : ViewModelBase
    {

        public ObservableCollection<CartItem> CartProduCartItems
            => GlobalCart.Instance.CartItems;


        public int MaxQuantity { get; set; } = 20;
        public int MinQuantity { get; set; }

        public double ProductsTotalPrice => CartProduCartItems.Sum(p => p.TotalPrice);
        public int ProductsItemsCount => CartProduCartItems.Count(p => p.TotalPrice >= 0);

        public CartPageViewModel(INavigationService navigationService, IDialogService dialogService)
            : base(navigationService, dialogService)
        {
            MessagingSubscriptions();
        }

        public ICommand IncrementQuantityCommand => new Command((product) =>
        {
            var cartItemProduct = (CartItem) product;

            if (cartItemProduct.Quantity >= MaxQuantity)
                cartItemProduct.Quantity = MaxQuantity;
            else
                cartItemProduct.Quantity = ++cartItemProduct.Quantity;

        });

        public ICommand DecrementQuantityCommand => new Command((product) =>
        {
            var cartItemProduct = (CartItem) product;

            if (cartItemProduct.Quantity <= MinQuantity)
                cartItemProduct.Quantity = MinQuantity;
            else
                cartItemProduct.Quantity = --cartItemProduct.Quantity;
            
        });

        private void MessagingSubscriptions()
        {
            MessagingCenter.Instance.Subscribe<GlobalCart>(this,"CartItemChanged", (sender) =>
            {
                RaisePropertyChanged(nameof(ProductsTotalPrice));
                RaisePropertyChanged(nameof(ProductsItemsCount));
            });
        }
    }
}
