﻿using Prism.Navigation;
using Prism.Services.Dialogs;
using Sokoeasy.Core.Services.Abstractions;
using Sokoeasy.Domain;
using Sokoeasy.Mobile.Messaging;
using Sokoeasy.Mobile.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sokoeasy.Mobile.ViewModels
{
    public class RegisterPageViewModel : ViewModelBase
    {
        private readonly IUserServices _userServices;
        private readonly IAuth _auth;

        private string _address;
        private string _fullName;
        private string _phoneNumber;

        public string FullName
        {
            get => _fullName;
            set => SetProperty(ref _fullName, value);
        }
        public string PhoneNumber
        {
            get => _phoneNumber;
            set => SetProperty(ref _phoneNumber, value);
        }
        public string Address
        {
            get => _address;
            set => SetProperty(ref _address, value);
        } 

        public RegisterPageViewModel(INavigationService navigationService, IDialogService dialogService,
                                     IUserServices userServices, IAuth auth): base(navigationService, dialogService)
        {
            MessagingSubscription();

            _auth = auth;
            _userServices = userServices;
        }

        public Command GoBackCommand => new Command(async () =>
        {
            await NavigationService.GoBackAsync();
        });

        public Command GoToVerifyCommand => new Command(() =>
        {
            Task[] tasks = new Task[2];

            var loginTask = _auth.Login(_phoneNumber);
            var navigationTask = NavigationService.NavigateAsync("VerificationPage");

            tasks[0] = loginTask;
            tasks[1] = navigationTask;

            Task.WhenAll(tasks);
        });

        private void MessagingSubscription()
        {
            MessagingCenter.Instance.Subscribe<VerificationCompleted>(this,MessagingTokens.AuthVerificationCompleted,
                onVerificationCompletedAsync);
        }

        private async void onVerificationCompletedAsync(VerificationCompleted obj)
        {
            User newUser = new User()
            {
                FullName = _fullName,
                PhoneNumber = _phoneNumber,
                Address = _address
            };

            var result = await _userServices.AddUserAsync(newUser,null);

            if (result == Core.AddState.Error)
            {
                await Application.Current.MainPage.DisplayAlert("Sokoeasy",
                    "Un problème de connexion au serveur est survenue.",
                    "Ok");

                return;
            }

            await Application.Current.MainPage.DisplayAlert("Sokoeasy",
                    "Votre compte été créer avec succès.",
                    "Ok");
        }
    }
}
