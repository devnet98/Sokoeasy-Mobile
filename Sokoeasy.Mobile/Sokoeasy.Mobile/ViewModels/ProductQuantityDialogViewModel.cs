﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services.Dialogs;
using Prism.Services.Dialogs.Popups;
using Rg.Plugins.Popup.Services;
using Sokoeasy.Mobile.Models;
using Xamarin.Forms;

namespace Sokoeasy.Mobile.ViewModels
{
    public class ProductQuantityDialogViewModel : BindableBase
    {
        private CartItem _product;

        public int MaxQuantity { get; set; } = 20;
        public int MinQuantity { get; set; }

        public CartItem Product
        {
            get => _product;

            set => SetProperty(ref _product, value);
        }

        public ICommand IncrementQuantityCommand => new Command(() =>
        {
            if (Product.Quantity >= MaxQuantity)
                Product.Quantity = MaxQuantity;
            else
                Product.Quantity = ++Product.Quantity;
        });

        public ICommand DecrementQuantityCommand => new Command(() =>
        {
            if (Product.Quantity <= MinQuantity)
                Product.Quantity = MinQuantity;
            else
                Product.Quantity = --Product.Quantity;
        });

        public ICommand AddToCartCommand => new Command(() =>
        {
            PopupNavigation.Instance.PopAsync(true);
        });

        public ProductQuantityDialogViewModel(CartItem product)
        {
            var selectedItem = GlobalCart.Instance.CartItems
                .FirstOrDefault(c => c.Product.Name == product.Product.Name);

            if (selectedItem == null)
            {
                GlobalCart.Instance.CartItems.Add(product);
                Product = product;
            }
            else
            {
                Product = selectedItem;
            }
        }

    }
}
