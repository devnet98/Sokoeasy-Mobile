﻿using Prism.Navigation;
using Prism.Navigation.Xaml;
using Prism.Services.Dialogs;
using Sokoeasy.Mobile.Enums;
using Sokoeasy.Mobile.Messaging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Sokoeasy.Mobile.ViewModels
{
    public class PresentationPageViewModel : ViewModelBase
    {
        public Command LoginCommand => new Command(async () =>
        {
           await NavigationService.NavigateAsync("LoginPage");
        });

        public Command RegisterCommand => new Command(async () =>
        {
            await Task.Delay(100);
            //await NavigationService.NavigateAsync("NavigationPage/RegisterPage");
            MessagingCenter.Send(this, MessagingTokens.TransitionMessage, TransitionType.SlideFromRight);
        });

        public PresentationPageViewModel(INavigationService navigationService, IDialogService dialogService)
            : base(navigationService, dialogService)
        {
        }
    }
}
