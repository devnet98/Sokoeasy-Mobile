﻿using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Sokoeasy.Mobile.ViewModels
{
    public class VerificationPageViewModel : ViewModelBase
    {
        public VerificationPageViewModel(INavigationService navigationService, IDialogService dialogService)
            : base(navigationService, dialogService)
        {
        }

        public Command GoBackCommand => new Command(async () =>
        {
            await NavigationService.GoBackAsync();
        });

        public Command OpenMarketCommand => new Command(async () =>
        {
            await NavigationService.NavigateAsync("MainPage");
        });
    }
}
