﻿using Prism.Navigation;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sokoeasy.Mobile.ViewModels
{
    public class PhoneVerificationPageViewModel : ViewModelBase
    {
        public PhoneVerificationPageViewModel(INavigationService navigationService, IDialogService dialogService)
            : base(navigationService, dialogService)
        {
        }
    }
}
