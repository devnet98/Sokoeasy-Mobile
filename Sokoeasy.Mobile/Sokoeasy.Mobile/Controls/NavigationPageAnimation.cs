﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Sokoeasy.Mobile.Enums;

namespace Sokoeasy.Mobile.Controls
{
    public class NavigationPageAnimation : NavigationPage
    {
        public static readonly BindableProperty TransitionTypeProperty =
            BindableProperty.Create("TransitionType", typeof(TransitionType), typeof(NavigationPageAnimation), TransitionType.SlideFromLeft);

        public TransitionType TransitionType
        {
            get { return (TransitionType)GetValue(TransitionTypeProperty); }
            set { SetValue(TransitionTypeProperty, value); }
        }

        public NavigationPageAnimation() : base()
        {
        }

        public NavigationPageAnimation(Page root) : base(root)
        {
        }
    }
}
