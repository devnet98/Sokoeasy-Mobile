﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Sokoeasy.Mobile.Controls
{
    public class CustomMaterialEntry : Entry
    {
        private Color _initialTextColor;
        public CustomMaterialEntry()
        {
            MeasureInvalidated += CustomMaterialEntry_MeasureInvalidated;
            Focused += CustomMaterialEntry_Focused;
            Unfocused += CustomMaterialEntry_Unfocused;
            PropertyChanged += CustomMaterialEntry_PropertyChanged;

            _initialTextColor = TextColor;
        }

        private void CustomMaterialEntry_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(TextColor) && TextColor != PlaceholderColor)
            {
                _initialTextColor = TextColor;
            }
        }

        private void CustomMaterialEntry_MeasureInvalidated(object sender, EventArgs e)
        {
            Text = ((Text == string.Empty) || (Text == null)) ? Placeholder : Text;
            TextColor = Text == Placeholder ? PlaceholderColor : TextColor;
        }

        private void CustomMaterialEntry_Unfocused(object sender, FocusEventArgs e)
        {
            Text = Text == string.Empty ? Placeholder : Text;
            TextColor = Text == Placeholder ? PlaceholderColor : TextColor;
        }

        private void CustomMaterialEntry_Focused(object sender, FocusEventArgs e)
        {
            Text = Text == Placeholder ? string.Empty : Text;
            TextColor = _initialTextColor;
        }
    }
}
