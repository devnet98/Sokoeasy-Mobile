﻿using System;
using Firebase;
using Android.Widget;
using Firebase.Auth;
using Xamarin.Essentials;
using static Firebase.Auth.PhoneAuthProvider;
using Xamarin.Forms;
using Sokoeasy.Mobile.Messaging;

namespace Sokoeasy.Mobile.Droid.Callbacks
{
    public class AuthCallbacks : OnVerificationStateChangedCallbacks
    {
        private string _verificationId;
        public override void OnVerificationCompleted(PhoneAuthCredential p0)
        {
            Toast.MakeText(Platform.CurrentActivity, "Connecter avec succes !", ToastLength.Long);

            System.Diagnostics.Debug.WriteLine($"CODE IS : {p0.SmsCode}");
            System.Diagnostics.Debug.WriteLine($"CRENDETIAL IS : {p0}");

            PhoneAuthCredential credential = GetCredential(_verificationId, p0.SmsCode);
            SignInWithPhoneAuthCredential(credential);

            MessagingCenter.Instance.Send(new VerificationCompleted(), MessagingTokens.AuthVerificationCompleted);
        }

        public override void OnVerificationFailed(FirebaseException p0)
        {
            Toast.MakeText(Platform.CurrentActivity, p0.ToString(), ToastLength.Long);
        }

        public override void OnCodeSent(string p0, ForceResendingToken p1)
        {
            base.OnCodeSent(p0, p1);
            _verificationId = p0;
        }

        private async void SignInWithPhoneAuthCredential(PhoneAuthCredential credential)
        {
            try
            {
                var authResult = await FirebaseAuth.Instance.SignInWithCredentialAsync(credential);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }
    }
}