﻿using System;
using Firebase.Auth;
using Xamarin.Essentials;
using Java.Util.Concurrent;
using System.Threading.Tasks;
using Sokoeasy.Mobile.Services.Interfaces;
using static Firebase.Auth.PhoneAuthProvider;
using Sokoeasy.Mobile.Droid.Callbacks;

namespace Sokoeasy.Mobile.Droid.PlatformServices
{
    public class AndroidPhoneAuth : IAuth
    {
        public Task Login(string phonenumber)
        {
            var callback = new AuthCallbacks();

            FirebaseAuth.Instance.LanguageCode = "fr";
            Instance.VerifyPhoneNumber(phonenumber, 2, TimeUnit.Minutes, Platform.CurrentActivity, callback);
            return Task.CompletedTask;
        }
    }
}