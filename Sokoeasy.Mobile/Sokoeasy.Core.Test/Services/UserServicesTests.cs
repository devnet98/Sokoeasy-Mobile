﻿using NUnit.Framework;
using Sokoeasy.Core.Services;
using Sokoeasy.Core.Services.Abstractions;
using Sokoeasy.Core.Services.FakeServices;
using Sokoeasy.Domain;
using System.Threading.Tasks;

namespace Sokoeasy.Core.Test.Services
{
    public class UserServicesTests
    {
        private IUserServices _userServices;

        [SetUp]
        public void Setup()
        {
            _userServices = new UserServices();
        }

        [Test]
        public async Task AddNewUserToFakeDbTest()
        {
            _userServices = new FakeUserServices();

            Domain.User newUser = new Domain.User()
            {
                Address = "test_addres",
                Email = "test@test",
                PhoneNumber = "test_phoneNumber",
                FullName = "test test",
                IsAdministrator = false
            };

            var result = await _userServices.AddUserAsync(newUser,null);

            Assert.IsTrue(result == AddState.Succeed || result == AddState.UserExist);
        }

        [Test]
        public async Task GetUsersFromFakeDbTest()
        {
            _userServices = new FakeUserServices();

            var result = await _userServices.GetUsersAsync();
            Assert.That(result != null && result.Count >= 0);
        }

        [Test]
        public async Task GetUserByPhoneNUmberFromFakeDbTest()
        {
            _userServices = new FakeUserServices();

            var result = await _userServices.GetUserByPhoneNumber("243820041966");
            Assert.That(result != null);
        }

        [Test]
        public async Task AddNewUserToFirebaseDbTest()
        {

            Domain.User newUser = new Domain.User()
            {
                Address = "test_addres",
                Email = "test@test",
                PhoneNumber = "test_phoneNumber",
                FullName = "test test",
                IsAdministrator = false
            };

            var result = await _userServices.AddUserAsync(newUser,null);

            Assert.IsTrue(result == AddState.Succeed || result == AddState.UserExist);
        }

        [Test]
        public async Task GetUsersFromFirebaseDbTest()
        {
            var result = await _userServices.GetUsersAsync();
            Assert.That(result != null && result.Count >= 0);
        }

        [Test]
        public async Task UpdateUserAsyncFromFirebaseDbTest()
        {
           var result = await _userServices.UpdateUserAsync("test_phoneNumber", nameof(User.Address), "Av, Likasi 7862");
           Assert.That(result == true);
        }

        [Test]
        public async Task GetUserByPhoneNUmberFromFirebaseFakeDbTest()
        {
            var result = await _userServices.GetUserByPhoneNumber("test_phoneNumber");
            Assert.That(result != null);
        }

    }
}
