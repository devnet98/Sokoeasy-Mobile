﻿using Firebase.Database;
using Firebase.Storage;
using Sokoeasy.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Sokoeasy.Core.Helpers
{
    public static class UserServicesHelpers
    {
        public static async Task<bool> IfUserExist(FirebaseClient firebaseDatabase, string numberPhone)
        {
            return (await GetUserKeyByNumberPhone(firebaseDatabase,numberPhone) != null) ? true : false;
        }
        public static async Task<string> GetUserKeyByNumberPhone(FirebaseClient firebaseDatabase, string numberPhone)
        {
            string firebaseObjectKey = null;

            try
            {
                var x = await firebaseDatabase.Child(nameof(User)).OnceAsync<User>();

                foreach (var item in x)
                {
                    if (item.Object.PhoneNumber == numberPhone)
                    {
                        firebaseObjectKey = item.Key;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            return firebaseObjectKey;
        }
        public static async Task<string> UploadImage(FirebaseStorage firebaseStorage, string filePath, string Childfolder, string fileName)
        {
            if (filePath == null || filePath == string.Empty) return await Task.FromResult(string.Empty);

            var stream = File.Open(filePath, FileMode.Open);

            var task = firebaseStorage
                .Child(Childfolder)
                .Child(fileName)
                .PutAsync(stream);

            return await task;
        }

    }
}
