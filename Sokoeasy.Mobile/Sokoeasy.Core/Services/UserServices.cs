﻿using System;
using System.Collections.Generic;
using System.Text;
using Firebase.Database;
using Firebase.Database.Query;
using System.Threading.Tasks;
using Sokoeasy.Domain;
using Sokoeasy.Core.Services.Abstractions;
using System.IO;
using Sokoeasy.Core.Helpers;

namespace Sokoeasy.Core.Services
{
    public class UserServices : BaseServices, IUserServices
    {

        public async Task<AddState> AddUserAsync(User entity, string imagePath)
        {
            bool result = false;

            string imageUploadedPath = await UserServicesHelpers.UploadImage(firebaseStorage, imagePath, "UserProdiles", Guid.NewGuid().ToString() + ".png");

            if (await UserServicesHelpers.IfUserExist(firebaseDatabase,entity.PhoneNumber))
            {
                return AddState.UserExist;
            }

            try
            {
                entity.Id = Guid.NewGuid().ToString();
                entity.ProfileImage = imageUploadedPath;
                result = await firebaseDatabase.Child(nameof(User)).PostAsync(entity) != null
                    ? true
                    : false;
            }
            catch (Exception)
            {
                return AddState.Error;
            }

            return  result == true ? AddState.Succeed : AddState.Error;
        }

        public async Task<bool> DeleteUserAsync(string phoneNumber)
        {
            string keyOfUser = await UserServicesHelpers.GetUserKeyByNumberPhone(firebaseDatabase,phoneNumber);

            try
            {
                await firebaseDatabase
                    .Child(nameof(User) + "/" + keyOfUser)
                    .DeleteAsync();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public async Task<User> GetUserByPhoneNumber(string numberPhone)
        {
            FirebaseObject<User> firebaseObject = null;

            try
            {
                var x = await firebaseDatabase.Child(nameof(User)).OnceAsync<User>();

                foreach (var item in x)
                {
                    if (item.Object.PhoneNumber == numberPhone)
                    {
                        firebaseObject = item;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }

            return firebaseObject != null ? firebaseObject.Object : null;
        }

        public async Task<List<User>> GetUsersAsync()
        {
            try
            {
                var usersFirebaseObjects = await firebaseDatabase.Child(nameof(User))
                               .OnceAsync<User>();

                List<User> users = new List<User>();

                foreach (var userFirebaseObject in usersFirebaseObjects)
                {
                    users.Add(userFirebaseObject.Object);
                }

                return users;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public async Task<bool> UpdateUserAsync(string numberPhone, string propertyName, string value)
        {
            try
            {
                await firebaseDatabase
                     .Child(nameof(User))
                     .Child(await UserServicesHelpers.GetUserKeyByNumberPhone(firebaseDatabase,numberPhone))
                     .Child(propertyName)
                     .PutAsync<string>(value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
