﻿using Firebase.Database.Query;
using Sokoeasy.Core.Services;
using Sokoeasy.Core.Services.Abstractions;
using Sokoeasy.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sokoeasy.Core.Services.FakeServices
{
    public class FakeUserServices : BaseServices, IUserServices
    {
        private List<User> fakeUsers;

        public FakeUserServices() => fakeUsers = new List<User>()
        {
            new User()
            {
                 Address = "Av, Lusonde 2628",
                 Email = "ken.kabange@gmail.com",
                 FullName = "Ken Mwepu",
                 Id = "09a0d96e-5acd-4b43-b089-48f7daab7bd4",
                 IsAdministrator = true,
                 PhoneNumber = "243820041966",
                 ProfileImage = ""
            }
        };

        public async Task<AddState> AddUserAsync(User entity, string imagePath)
        {
            try
            {
                entity.Id = Guid.NewGuid().ToString();
                entity.ProfileImage = imagePath;
                fakeUsers.Add(entity);
            }
            catch (Exception)
            {
                return await Task.FromResult(AddState.Error);
            }

            return await Task.FromResult(AddState.Succeed);
        }

        public Task<bool> DeleteUserAsync(string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task<User> GetUserByPhoneNumber(string numberPhone)
        {
            return Task.FromResult(fakeUsers.FirstOrDefault(x => x.PhoneNumber == numberPhone));
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await Task.FromResult(fakeUsers.ToList());
        }

        public Task<bool> UpdateUserAsync(string numberPhone, string propertyName, string value)
        {
            throw new NotImplementedException();
        }
    }
}
