﻿using Firebase.Database;
using Firebase.Storage;
using Sokoeasy.Core.Services.Abstractions;
using Sokoeasy.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sokoeasy.Core.Services
{
    public class BaseServices
    {
        protected FirebaseClient firebaseDatabase { get; private set; }
        protected FirebaseStorage firebaseStorage { get; private set; }
        public BaseServices()
        {
            firebaseDatabase = new FirebaseClient("https://sokoeasy-9f5e1.firebaseio.com/");
            firebaseStorage = new FirebaseStorage("sokoeasy-9f5e1.appspot.com");
        }
    }
}
