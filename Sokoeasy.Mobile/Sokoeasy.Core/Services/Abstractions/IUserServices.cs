﻿using Sokoeasy.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sokoeasy.Core.Services.Abstractions
{
    public interface IUserServices
    {
        Task<AddState> AddUserAsync(User entity, string imagePath);

        Task<List<User>> GetUsersAsync();

        Task<bool> DeleteUserAsync(string phoneNumber);

        Task<bool> UpdateUserAsync(string numberPhone, string propertyName, string value);

        Task<User> GetUserByPhoneNumber(string numberPhone);
    }
}
